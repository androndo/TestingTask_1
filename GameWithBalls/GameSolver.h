#pragma once

#include "GameDesk.h"
#include "Matrix.h"

using GameDeskObjects::CellPosition;
using GameDeskObjects::GameDesk;
using GameDeskObjects::Move;
using GameDeskObjects::Path;

// todo: ��������� � GameDeskObjects
using Hole = GameDeskObjects::ObjectWithId_IntoCell;
using Ball = GameDeskObjects::ObjectWithId_IntoCell;
using Wall = GameDeskObjects::Object_BetweenCells;

/** @brief GameSolver ������� ������� ���� */
class GameSolver
{
public:
	explicit GameSolver(const GameDesk& desk)
		: desk(desk)
	{}

	virtual ~GameSolver() {}

	/** @brief �������� ����������� ���� ��� ����� � ����� */
	std::vector<Path> getMinimalPathsToWin() const;

	/** @warning ������ ����, �� ���� �� ����� � ����������,
		����� �������� ������� � protected
		� ����� � ������ ����������� ����� ��������. */

	/** @brief �������� �������� �������� ��� ����������� ���� */
	Matrix<size_t> getGradientOfMoving(const Ball& ball) const;

	/** @brief �������� ��������� ����� ��������� ���� ����� ����� ����� */
	int calculateCostAfterMove(Move, const GameDesk& desk) const;

	/** @brief �������� ���� ��� ����� � ����� */
	std::vector<Path> getAllPathsToWin(const GameDesk& desk) const;

private:
	GameDesk desk;
	
	/** @brief ���������� ������� (��������) ��������� ����*/
	Matrix<size_t> calculateCostsByWaveAlgorythm(CellPosition destination) const;

	/** @biref ��������� ��������� ������ � ������� ��������� ���� */
	void fillCostNearFromPos(CellPosition pos, size_t currentCost, Matrix<size_t>& result) const;

	/** @brief �������� �� ���������� � from ��������� move */
	bool isAvaliableStepTo(CellPosition from, Move move) const;
};


