#include "GameDesk.h"
#include "CommonFunctions.h"

namespace GameDeskObjects
{
	using namespace std;

	GameDesk::GameDesk(): N(0) {}

	GameDesk::~GameDesk() {}

	void GameDesk::setStep(Move move)
	{
		using Ball = ObjectWithId_IntoCell;
		vector<Ball> ballsAtNewPositions;
		using Hole = ObjectWithId_IntoCell;
		vector<Hole> nonClosedHoles;
		for (const auto& ball : balls)
		{
			const auto newPosition = common::getMaxAvaliableStep(ball.position, move, *this);
			ObjectWithId_IntoCell hole;
			if (getHoleByBall(ball, hole))
			{
				if (hole.position != newPosition)
				{
					/** @note ��� ��� � ���� */
					ballsAtNewPositions.emplace_back(Ball(ball.id, newPosition));
					nonClosedHoles.push_back(hole);
				}
			}
		}
		setBalls(ballsAtNewPositions);
		setHoles(nonClosedHoles);
	}

	bool GameDesk::getHoleByBall(const ObjectWithId_IntoCell& ball, ObjectWithId_IntoCell& result) const
	{
		for (const auto& hole : holes)
		{
			if (hole.id == ball.id)
			{
				result = hole;
				return true;
			}
		}
		return false;
	}

	bool GameDesk::getBallById(size_t ballId, ObjectWithId_IntoCell& result) const
	{
		for (const auto& ball : balls)
		{
			if (ball.id == ballId)
			{
				result = ball;
				return true;
			}
		}
		return false;
	}
} // namespace GameDeskObjects