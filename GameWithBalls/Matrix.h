#pragma once

#include <vector>
#include <string>

/** @brief Matrix ������� �������
@param T - ��� ��������� */
template <typename T>
class Matrix
{
	std::vector<std::vector<T> > values;

public:
	Matrix(){}

	Matrix(size_t size)
	{
		std::vector<T> inner;
		inner.assign(size, 1000);
		values.assign(size, inner);
	}

	///Matrix(const std::initializer_list<std::initializer_list<T> >& matrix) :
	Matrix(const std::vector<std::vector<T> >& matrix) :
		values(matrix)
	{}

	/** @warning debug only! */
	/** @brief ����������� � ������ */
	std::string toString() const
	{
		std::string result;
		for (const auto& array : this->values)
		{
			for (const auto& element : array)
			{
				result.append(std::to_string(element));
			}
		}
		return result;
	}

	/** @brief ������ �������� �������� */
	void setValue(int i, int j, T value)
	{
		values.at(i).at(j) = value;
	}

	/** @brief �������� �������� �������� */
	T getValue(int i, int j) const
	{
		return values.at(i).at(j);
	}

	inline size_t getSize()
	{
		return values.size();
	}

	template <typename U>
	friend bool operator==(const Matrix<U>& left, const Matrix<U>& right);
};

/** @brief ��������� ��������� */
template<class T>
bool operator==(const Matrix<T>& left, const Matrix<T>& right)
{
	return (left.values == right.values);
}