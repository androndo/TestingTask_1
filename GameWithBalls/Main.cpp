#include <iostream>
#include <fstream>
#include <string>
#include "GameDeskFactory.h"
#include "GameSolver.h"
#include "CommonFunctions.h"

using namespace std;

int main(int argc, char** argv)
{	
	if (argc != 2)
	{
		cout << "Incorrect format!" << endl
			<< "GameWithBalls <path to input file>" << endl;
	}
	else
	{
		/** @note ��������� ���� ������� ���� */
		ifstream configFile(argv[1], ios_base::in);
		if (!configFile)
		{
			cout << "File not found!" << endl;
		}

		/** @note ������ ����� ���� */
		GameDesk desk;
		try
		{
			desk = GameDeskFactory::createGameDesk(configFile);
		}
		catch(logic_error& ex)
		{
			cout << "Program aborted, because: " << ex.what() << endl;
		}

		/** @note ������ */
		GameSolver solver(desk);
		const auto paths = solver.getMinimalPathsToWin();

		/** @note ������� ��������� */
		for (const auto& path : paths)
		{
			const auto stringWithPath = common::pathToString(path);
			cout << stringWithPath << endl;
		}
	}
	return 0;
}
