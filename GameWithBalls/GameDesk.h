#pragma once
#include <vector>

namespace GameDeskObjects
{
	/** @brief ������� ������ */
	using CellPosition = std::pair<size_t, size_t>;

	/** @brief ObjectWithId_IntoCell ������������ ������ � ������ */
	struct ObjectWithId_IntoCell
	{
		ObjectWithId_IntoCell() : id(), position() {}

		ObjectWithId_IntoCell(size_t id, CellPosition position) : 
			id(id)
			,position(position)
		{}

		size_t id;
		CellPosition position;
	};

	/** @brief Object_BetweenCells ������������ ������ ����� �������� */
	struct Object_BetweenCells
	{
		Object_BetweenCells(CellPosition start, CellPosition end)
			: startPosition(start), endPosition(end)
		{
			if (startPosition.first == endPosition.first)
			{
				isHorizontal = false;
			}
			else
			{
				if (startPosition.second == endPosition.second)
				{
					isHorizontal = true;
				}
				else
				{
					throw std::logic_error("not correct creation of object between cells");
				}
			}
		}

		CellPosition startPosition;
		CellPosition endPosition;
		bool isHorizontal;
	};

	using std::vector;

	/** @brief ��� �������� �� ����� */
	enum Move
	{
		left, right, up, down
	};

	/** @brief ���� �������� */
	using Path = std::vector<Move>;

	using Wall = Object_BetweenCells;

	/**@brief GameDesk ������������ ����� ���� */
	class GameDesk
	{
	public:
		GameDesk();
		virtual ~GameDesk();

		/** @note ������� �������� ������ ��������, � �� ��������� � ���. */
		void setSizeOfDeskSquare(const size_t& N);

		void setBalls(const vector<ObjectWithId_IntoCell>&);
		void setHoles(const vector<ObjectWithId_IntoCell>&);
		void setWalls(const vector<Object_BetweenCells>&);

		void setStep(Move move);

		vector<ObjectWithId_IntoCell> getBalls() const;
		vector<ObjectWithId_IntoCell> getHoles() const;
		vector<Object_BetweenCells> getWalls() const;

		/** @brief �������� ������ ����� */
		size_t getSizeOfDeskSquare() const;

		/** @brief �������� ����, ��������������� ����
			@return false ����� �� �������
		*/
		bool getHoleByBall(const ObjectWithId_IntoCell& ball, ObjectWithId_IntoCell& hole) const;
		bool getBallById(size_t ballId, ObjectWithId_IntoCell& hole) const;

	private:
		vector<ObjectWithId_IntoCell> balls;
		vector<ObjectWithId_IntoCell> holes;
		vector<Object_BetweenCells> walls;
		size_t N;
	};

	inline void GameDesk::setSizeOfDeskSquare(const size_t& N)
	{
		this->N = N;
	}

	inline void GameDesk::setBalls(const vector<ObjectWithId_IntoCell>& balls)
	{
		this->balls = balls;
	}

	inline void GameDesk::setHoles(const vector<ObjectWithId_IntoCell>& holes)
	{
		this->holes = holes;
	}

	inline void GameDesk::setWalls(const vector<Object_BetweenCells>& walls)
	{
		this->walls = walls;
	}

	inline vector<Object_BetweenCells> GameDesk::getWalls() const
	{
		return walls;
	}

	inline vector<ObjectWithId_IntoCell> GameDesk::getBalls() const
	{
		return balls;
	}

	inline vector<ObjectWithId_IntoCell> GameDesk::getHoles() const
	{
		return holes;
	}

	inline size_t GameDesk::getSizeOfDeskSquare() const
	{
		return N;
	}
} // namespace GameDeskObjects
