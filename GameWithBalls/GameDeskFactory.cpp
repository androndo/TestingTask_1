#include "GameDeskFactory.h"

using namespace GameDeskObjects;

const std::string errorMessage = "Expected count of parameters is not equal to actual value";

vector<ObjectWithId_IntoCell> createCellObjects(const vector<int>& parameters, int offset, int countOfObjects)
{
	vector<ObjectWithId_IntoCell> objects;
	int counter = 0;
	const int sizeOfObjectParameters = 2;
	for (auto i = offset; i < (offset + countOfObjects * sizeOfObjectParameters); i += sizeOfObjectParameters)
	{
		/** @note �������� -1 �� ���������, �.�. � ������ ���������� �� ���� � 1 */
		objects.emplace_back(ObjectWithId_IntoCell(counter, { parameters[i] - 1, parameters[i + 1] - 1 }));
		counter++;
	}
	return objects;
}

vector<Object_BetweenCells> createWallObjects(const vector<int>& parameters, int offset, int countOfObjects)
{
	vector<Object_BetweenCells> walls;
	int counter = 0;
	const int sizeOfObjectParameters = 4;
	for (auto i = offset; i < (offset + countOfObjects * sizeOfObjectParameters); i += sizeOfObjectParameters)
	{
		/** @note �������� -1 �� ���������, �.�. � ������ ���������� �� ���� � 1 */
		walls.emplace_back(
			Object_BetweenCells(
				{ parameters[i] - 1, parameters[i + 1] - 1 },
				{ parameters[i + 2] - 1, parameters[i + 3] - 1 }
		));
		counter++;
	}
	return walls;
}

GameDesk GameDeskFactory::createGameDesk(std::istream& config)
{
	vector<int> gameParameters;
	int tmp;
	while (!config.eof())
	{
		config >> tmp;
		gameParameters.push_back(tmp);
	}

	/** @note �������� ���-�� N,K,M */
	const int headSize = 3;
	if (gameParameters.size() < headSize)
	{
		throw std::logic_error(errorMessage);
	}

	/** @note ����������� ������� ����� */
	const int N = gameParameters[0];
	/** @note ����� �������/��� */
	const int K = gameParameters[1];
	/** @note ����� ������ */
	const int M = gameParameters[2];

	const int expectedCountOfParameters = headSize + K * 4 + M * 4;
	if (gameParameters.size() != expectedCountOfParameters)
	{
		throw std::logic_error(errorMessage);
	}

	GameDesk result;
	result.setSizeOfDeskSquare(static_cast<size_t>(N));
	result.setBalls(createCellObjects(gameParameters, headSize, K));
	result.setHoles(createCellObjects(gameParameters, headSize + K * 2, K));
	result.setWalls(createWallObjects(gameParameters, headSize + K * 4, M));
	return result;
}