#include "GameSolver.h"
#include <stdexcept>
#include <set>
#include "CommonFunctions.h"

using GameDeskObjects::Object_BetweenCells;
using namespace std;

int GameSolver::calculateCostAfterMove(Move move, const GameDesk& desk) const
{
	int totalCost = 0;
	for (const auto& ball : desk.getBalls())
	{
		const auto gradientMatrix = getGradientOfMoving(ball);
		const auto futurePosition = common::getMaxAvaliableStep(ball.position, move, desk);
		auto A = gradientMatrix.getValue(futurePosition.first, futurePosition.second);
		auto B = gradientMatrix.getValue(ball.position.first, ball.position.second);
		bool needAddNegative = false;
		if (B < A)
		{
			/** @note ������, �������, �.�. ���� ���� ����� ��� �������������*/
			needAddNegative = true;
			swap(A, B);
		}
		const int deltaCost = A - B;
		totalCost += (needAddNegative ? -deltaCost : deltaCost);
	}
	return totalCost;
}

vector<Path> GameSolver::getAllPathsToWin(const GameDesk& desk) const
{
	vector<Path> result;
	if (desk.getBalls().size() <= 0)
	{
		result.emplace_back(Path());
		return result;
	}

	const set<Move> directions = { Move::left, Move::right, Move::up, Move::down };
	for (const auto& direction : directions)
	{
		const auto cost = calculateCostAfterMove(direction, desk);
		if (cost < 0)
		{
			auto movedDesk = desk;
			movedDesk.setStep(direction);

			for (const Path& tail : getAllPathsToWin(movedDesk))
			{
				Path head;
				head.push_back(direction);
				head.insert(end(head), begin(tail), end(tail));
				result.push_back(head);
			}
		}
	}
	return result;
}

Matrix<size_t> GameSolver::calculateCostsByWaveAlgorythm(CellPosition destination) const
{
	Matrix<size_t> matrix(desk.getSizeOfDeskSquare());
	fillCostNearFromPos(destination, 0, matrix);
	return matrix;
}

void GameSolver::fillCostNearFromPos(CellPosition pos, size_t currentCost, Matrix<size_t>& result) const
{
	if (result.getValue(pos.first, pos.second) > currentCost)
	{
		/** @note ��������� ������� ��������� */
		result.setValue(pos.first, pos.second, currentCost);
	}
	else
	{
		return;
	}

	using std::pair;
	using Offset = pair<int, int>;
	const set<pair<Move, Offset> > directionsWithOffsets
	{
		{ Move::left, { -1, 0 } },
		{ Move::right, { 1, 0 } },
		{ Move::up, { 0, -1 } },
		{ Move::down, { 0, 1 } }
	};

	for (const auto& directionWithOffset : directionsWithOffsets)
	{
		if (isAvaliableStepTo(pos, directionWithOffset.first))
		{
			const Offset& offset = directionWithOffset.second;
			fillCostNearFromPos({ pos.first + offset.first, pos.second + offset.second }, currentCost + 1, result);
		}
	}
}

bool GameSolver::isAvaliableStepTo(CellPosition from, Move move) const
{
	return (from != common::getMaxAvaliableStep(from, move, desk));
}

vector<Path> GameSolver::getMinimalPathsToWin() const
{
	const size_t NonInitializedMinimalSize = 1000;
	size_t currentMinimalSize = NonInitializedMinimalSize;
	vector<Path> minimalPaths;
	for (const auto& path : getAllPathsToWin(desk))
	{
		const auto currentPathSize = path.size();
		if (currentPathSize < currentMinimalSize)
		{
			currentMinimalSize = currentPathSize;
			minimalPaths.clear();
			
		}
		if (currentPathSize == currentMinimalSize)
		{
			minimalPaths.push_back(path);
		}
	}
	return minimalPaths;
}

Matrix<size_t> GameSolver::getGradientOfMoving(const Ball& ball) const
{
	Hole destination;
	if (!desk.getHoleByBall(ball, destination))
	{
		throw logic_error("no one hole for ball with id = " + to_string(ball.id));
	}
	return calculateCostsByWaveAlgorythm(destination.position);
}
