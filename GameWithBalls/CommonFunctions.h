#pragma once
#include "GameDesk.h"

namespace common
{
	using GameDeskObjects::CellPosition;
	using GameDeskObjects::Move;
	using GameDeskObjects::Wall;
	using GameDeskObjects::GameDesk;

	/** @brief �������� �� �������� �������������� */
	static bool isHorizontalMoving(Move move)
	{
		return move == Move::right || move == Move::left;
	}

	/** @brief �������� ������������ ��� �� ����������� �� ����� desk � ��������� move */
	CellPosition getMaxAvaliableStep(const CellPosition& from, Move move, const GameDesk& desk);

	/** @brief �������� ��� ������ �� ����� ����� */
	bool isCellsOnLine(const CellPosition& A, const CellPosition& B, bool isHorizontalLine);

	/** @brief �������� � ���, ��� ������ �� ����� � ������� position */
	bool isWallOnLine(const Wall& wall, const CellPosition& position, bool checkThatHorizontalLine);

	/** @brief ������� � ������ ���� */
	std::string pathToString(const GameDeskObjects::Path& path);
} // namespace common