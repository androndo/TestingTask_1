#include "CommonFunctions.h"
#include <algorithm>

namespace common
{
	using namespace GameDeskObjects;
	using namespace std;

	const std::string errorMessage_UnexpectedMoveParameter = "Move parameter is troubled!";

	/** @brief ��������� ���������� distance �� �������� move � ����� from */
	CellPosition incrementPositionByMove(const CellPosition& from, Move move, int distance)
	{
		switch (move)
		{
		case left:
		{
			return CellPosition(from.first - distance, from.second);
		}

		case right:
		{
			return CellPosition(from.first + distance, from.second);
		}

		case up:
		{
			return CellPosition(from.first, from.second - distance);
		}

		case down:
		{
			return CellPosition(from.first, from.second + distance);
		}

		default:
		{
			throw invalid_argument(errorMessage_UnexpectedMoveParameter);
		}
		}
	}

	/** @brief �������� ������� ���� ����� ��� �������� move */
	size_t getDeskBorderByMove(const GameDesk& desk, Move move)
	{
		switch (move)
		{
		case Move::left:
		case Move::up:
		{
			return 0;
		}

		case Move::right:
		case Move::down:
		{
			return desk.getSizeOfDeskSquare() - 1;
		}

		default:
		{
			throw std::invalid_argument(errorMessage_UnexpectedMoveParameter);
		}
		}
	}

	/** @brief �������� ��������� �� ����� ��� �������� move.
	@return ���� ������ "�����", ����� ����� ������������� ��������. */
	int getDistanceByMove(const Object_BetweenCells& wall, const CellPosition& pos, Move move)
	{
		int result = 0;
		switch (move)
		{
		case left:
		{
			auto xOfWall = max(wall.startPosition.first, wall.endPosition.first);
			result = pos.first - xOfWall;
		}
		break;

		case right:
		{
			auto xOfWall = min(wall.startPosition.first, wall.endPosition.first);
			result = xOfWall - pos.first;
		}
		break;

		case up:
		{
			auto yOfWall = max(wall.startPosition.second, wall.endPosition.second);
			result = pos.second - yOfWall;
		}
		break;

		case down:
		{
			auto yOfWall = min(wall.startPosition.second, wall.endPosition.second);
			result = yOfWall - pos.second;
		}
		break;

		default:
			break;
		}
		return result;
	}

	/** @brief �������� ��������� �� ������� � ������ ��� �������� move.
	@return ���� ������ "�����", ����� ����� ������������� ��������. */
	int getDistanceByMove(const ObjectWithId_IntoCell& object, const CellPosition& pos, Move move)
	{
		int result = 0;
		const auto xOfObject = object.position.first;
		const auto yOfObject = object.position.second;
		switch (move)
		{
		case left:
		{
			result = pos.first - xOfObject;
		}
		break;

		case right:
		{
			result = xOfObject - pos.first;
		}
		break;

		case up:
		{
			result = pos.second - yOfObject;
		}
		break;

		case down:
		{
			result = yOfObject - pos.second;
		}
		break;

		default:
			break;
		}
		return result;
	}

	CellPosition getMaxAvaliableStep(const CellPosition& from, Move move, const GameDesk& desk)
	{
		const auto isHorizontalMove = isHorizontalMoving(move);
		const int NonInitializedMinDistance = 1000;
		auto minimalDistanceToObstruction = NonInitializedMinDistance;

		/** @note ���� ��������� ����� */
		for (const auto& wall : desk.getWalls())
		{
			if (isWallOnLine(wall, from, isHorizontalMove))
			{
				const auto distance = getDistanceByMove(wall, from, move);
				if ((distance >= 0) && (distance < minimalDistanceToObstruction))
				{
					minimalDistanceToObstruction = distance;
				}
			}
		}

		/** @note ���� ��������� ����� */
		for (const auto& hole : desk.getHoles())
		{
			if (hole.position == from)
			{
				/** @note �� ��������� ����� � ������� �����, �.�. �������� ��� ������ �� ����� ���� */
				break;
			}
			if (isCellsOnLine(hole.position, from, isHorizontalMove))
			{
				const auto distance = getDistanceByMove(hole, from, move);
				if ((distance >= 0) && (distance < minimalDistanceToObstruction))
				{
					minimalDistanceToObstruction = distance;
				}
			}
		}

		if (NonInitializedMinDistance == minimalDistanceToObstruction)
		{
			/** @note ����� ����������� �� ���� �������, ���� ������ */
			const auto borderPosition = getDeskBorderByMove(desk, move);
			return (isHorizontalMove ?
				CellPosition(borderPosition, from.second)
				: CellPosition(from.first, borderPosition));
		}

		return incrementPositionByMove(from, move, minimalDistanceToObstruction);
	}

	bool isCellsOnLine(const CellPosition& A, const CellPosition& B, bool isHorizontalLine)
	{
		return isHorizontalLine ? (A.second == B.second) : (A.first == B.first);
	}

	bool isWallOnLine(const Wall& wall, const CellPosition& position, bool checkThatHorizontalLine)
	{
		return (checkThatHorizontalLine == wall.isHorizontal)
			&& (isCellsOnLine(wall.startPosition, position, checkThatHorizontalLine));
	}

	/** @brief ������������� �������� � ��� ����� */
	char getCharForMove(Move move)
	{
		switch (move)
		{
		case Move::left:	return 'E';
		case Move::right:	return 'W';
		case Move::up:		return 'N';
		case Move::down:	return 'S';
		default:			throw std::invalid_argument(errorMessage_UnexpectedMoveParameter);
		}
	}

	string pathToString(const Path& path)
	{
		string pathString;
		for (const auto& move : path)
		{
			pathString += getCharForMove(move);
		}
		return pathString;
	}
} // namespace common