#include <gtest\gtest.h>
#include "GameDeskFactory.h"
#include "Common.h"

namespace test
{
	/** @brief GameDeskFactoryTests ����� ������ �� �������� ������� ����� (�� �������) */
	class GameDeskFactoryTests : public ::testing::Test
	{
	public:
		static GameDeskObjects::GameDesk createDeskFromSample()
		{
			return GameDeskFactory::createGameDesk(std::istringstream(getSampleConfig()));
		}
	};

	TEST_F(GameDeskFactoryTests, Initialize)
	{
		ASSERT_NO_THROW(createDeskFromSample());
	}
	
	TEST_F(GameDeskFactoryTests, ParseSizeOfDesk)
	{
		auto game = createDeskFromSample();
		ASSERT_EQ(4, game.getSizeOfDeskSquare());
	}

	TEST_F(GameDeskFactoryTests, CreatedAllBalls)
	{
		auto game = createDeskFromSample();
		ASSERT_EQ(2, game.getBalls().size());
	}

	TEST_F(GameDeskFactoryTests, CreatedAllHoles)
	{
		auto game = createDeskFromSample();
		ASSERT_EQ(2, game.getHoles().size());
	}

	TEST_F(GameDeskFactoryTests, CreatedAllwalls)
	{
		auto game = createDeskFromSample();
		ASSERT_EQ(2, game.getWalls().size());
	}
}