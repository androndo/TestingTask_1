#include "GameSolverTests_Base.h"

namespace test
{
	/** @brief GameSolver_getGradient_Tests ����� ������ �� ��������� ��������� �������� ���� �� ���� */
	class GameSolver_getGradient_Tests : public GameSolverTests_Base
	{};

	TEST_F(GameSolver_getGradient_Tests, getGradientOfMoving_ThrowsExceptionWhenNoBall)
	{
		// Arrange
		auto solver = createSolverWithDesk(createFakeDeskWithoutWalls());

		// Act & Assert
		ASSERT_THROW(solver.getGradientOfMoving(Ball()), std::logic_error);
	}

	TEST_F(GameSolver_getGradient_Tests, SimpleDesk)
	{
		// Arrange
		auto desk = createFakeDeskWithoutWalls();
		const auto ball = Ball();
		desk.setHoles({ Hole(ball.id, ball.position) });
		auto solver = createSolverWithDesk(desk);

		// Act
		const auto gradient = solver.getGradientOfMoving(ball);

		// Assert
		const auto expectedMatrix = Matrix<size_t>(
			{	
				{ 0, 1 },
				{ 1, 2 }
			}
		);
		ASSERT_EQ(expectedMatrix, gradient);
	}

	TEST_F(GameSolver_getGradient_Tests, SimpleDesk2)
	{
		// Arrange
		auto desk = createFakeDeskWithoutWalls();
		const auto ball = Ball(2, CellPosition{1, 1});
		desk.setHoles({ Hole(ball.id, ball.position) });
		auto solver = createSolverWithDesk(desk);

		// Act
		const auto gradient = solver.getGradientOfMoving(ball);

		// Assert
		const auto expectedMatrix = Matrix<size_t>(
			{
				{ 2, 1 },
				{ 1, 0 }
			}
		);
		ASSERT_EQ(expectedMatrix, gradient);
	}
} // namespace test