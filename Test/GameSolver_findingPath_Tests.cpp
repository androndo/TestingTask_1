#include "GameSolverTests_Base.h"

namespace test
{
	/** @brief GameSolver_findingPath_Tests ����� ������ �� ����� ���� ��� ���� �� ���� */
	class GameSolver_findingPath_Tests : public GameSolverTests_Base
	{};

	TEST_F(GameSolver_findingPath_Tests, CalculationCostOfMovingForOneBall)
	{
		// Arrange
		auto desk = createFakeDeskWithoutWalls();
		const auto ball = Ball(123, {1, 1});
		desk.setBalls({ ball });
		desk.setHoles({ Hole(ball.id, {0,0}) });
		auto solver = createSolverWithDesk(desk);

		// Act
		const auto result = solver.calculateCostAfterMove(Move::left, desk);

		// Assert
		ASSERT_EQ(-1, result);
	}

	TEST_F(GameSolver_findingPath_Tests, CalculationCostOfMovingForOneBall2)
	{
		// Arrange
		auto desk = createFakeDeskWithoutWalls();
		const auto ball = Ball(123, { 0, 1 });
		desk.setBalls({ ball });
		desk.setHoles({ Hole(ball.id,{ 0,0 }) });
		auto solver = createSolverWithDesk(desk);

		// Act
		const auto result = solver.calculateCostAfterMove(Move::right, desk);

		// Assert
		ASSERT_EQ(1, result);
	}

	TEST_F(GameSolver_findingPath_Tests, CalculationOfMinimalPathsForOneBall)
	{
		// Arrange
		auto desk = createFakeDeskWithoutWalls();
		const auto ball = Ball(123, { 1, 1 });
		desk.setBalls({ ball });
		desk.setHoles({ Hole(ball.id,{ 0,0 }) });
		auto solver = createSolverWithDesk(desk);

		// Act
		const auto result = solver.getAllPathsToWin(desk);

		// Assert
		ASSERT_EQ(2, result.size());
	}
}