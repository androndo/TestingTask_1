#pragma once
#include <string>

namespace test
{
	/** @note
	�� ���� ��������� ������� ���� � �������:
	N K M BX1 BY1 BX2 BY2 .. BXK BYK HX1 HY1 .. HXK HYK WL1 WT1 WR1 WB1 � WLM WTM WRM WBM
	���:
	N � ����������� �����
	K � ���������� �������, ����� ���������� �����
	BX1 BY1 � ���������� ������� ������ (���������� �� 1 �� N)
	BX2 BY2 � ���������� ������� ������
	BXK BYK � ���������� ���������� ������
	HX1 HY1 � ���������� ������ ����� (���������� �� 1 �� N)
	HXK HYK � ���������� ��������� �����
	WL1 WT1 WR1 WB1 � ���������� ������ ������.
	������ ������� ����� �������� ���� ������ �WL1 WT1� - ������ ������ � �WR1 WB1� - ������ ������.
	������ ����� ����� ����.
	WLM WTM WRM WBM � ���������� ��������� ������.
	*/
	inline std::string getSampleConfig()
	{
		return std::string("4 2 2 2 2 1 4 1 1 4 3 1 2 1 3 3 2 4 2");
	};
} // namespace test