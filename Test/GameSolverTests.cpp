#include "GameSolverTests_Base.h"
#include "CommonFunctions.h"

namespace test
{
	/** @brief GameSolverTests ����� ������ �� ��������� �-� */
	class GameSolverTests : public GameSolverTests_Base
	{};

	TEST_F(GameSolverTests, Initialize)
	{
		ASSERT_NO_THROW(GameSolver(createFakeDeskWithoutWalls()));
	}

	//// todo: � ���� �����
	using common::getMaxAvaliableStep;
	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnLeftBorder)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 1,0 }, Move::left, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 0, 0 }), step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnLeftBorder2)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 0,0 }, Move::left, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 0, 0 }), step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnRightBorder)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 0,0 }, Move::right, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 1, 0 }), step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnTopBorder)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 1,1 }, Move::up, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 1, 0 }), step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnBottomBorder)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 0,0 }, Move::down, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 0, 1 }), step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenNoWall_MustReturnTopBorder2)
	{
		const auto desk = createFakeDeskWithoutWalls();

		const auto step = getMaxAvaliableStep({ 0,0 }, Move::up, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 0, 0 }), step);
	}
	//// ����� ������

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenAllWalls_MustReturnCurrentPosition)
	{
		const auto desk = createFakeDeskWithWalls();
		const auto position = GameDeskObjects::CellPosition({ 0, 0 });
		const auto step = getMaxAvaliableStep(position, Move::down, desk);
		ASSERT_EQ(position, step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenAllWalls_MustReturnCurrentPosition2)
	{
		const auto desk = createFakeDeskWithWalls();
		const auto position = GameDeskObjects::CellPosition({ 1, 1 });
		const auto step = getMaxAvaliableStep(position, Move::up, desk);
		ASSERT_EQ(position, step);
	}

	TEST_F(GameSolverTests, getMaxAvaliableStep_WhenHasWallsOnLine_MustReturnNearest)
	{
		const auto desk = createFakeDeskWithWalls();
		const auto step = getMaxAvaliableStep({ 0, 1 }, Move::left, desk);
		ASSERT_EQ(GameDeskObjects::CellPosition({ 0, 1 }), step);
	}

	/// todo: � ��������� ����� ��������������� �-�
	using common::isWallOnLine;
	TEST_F(GameSolverTests, isWallOnLine_checkVerticalLine1_VerticalWallOnLine)
	{
		using GameDeskObjects::Object_BetweenCells;
		using GameDeskObjects::CellPosition;
		const bool result = isWallOnLine(Object_BetweenCells(CellPosition({ 0,0 }), CellPosition({ 0,1 })), CellPosition({ 0,1 }), false);
		ASSERT_TRUE(result);
	}

	TEST_F(GameSolverTests, isWallOnLine_checkVerticalLine2_WhenWallNonVertical)
	{
		using GameDeskObjects::Object_BetweenCells;
		using GameDeskObjects::CellPosition;
		const bool result = isWallOnLine(Object_BetweenCells(CellPosition({ 0,1 }), CellPosition({ 1,1 })), CellPosition({ 0,1 }), false);
		ASSERT_FALSE(result);
	}

	TEST_F(GameSolverTests, isWallOnLine_checkVerticalLine3_WhenWallNonVertical)
	{
		using GameDeskObjects::Object_BetweenCells;
		using GameDeskObjects::CellPosition;
		const bool result = isWallOnLine(Object_BetweenCells(CellPosition({ 0,1 }), CellPosition({ 1,1 })), CellPosition({ 1,1 }), false);
		ASSERT_FALSE(result);
	}

	TEST_F(GameSolverTests, isWallOnLine_checkVerticalLine3_WhenVerticalWallOnOtherLine)
	{
		using GameDeskObjects::Object_BetweenCells;
		using GameDeskObjects::CellPosition;
		const bool result = isWallOnLine(Object_BetweenCells(CellPosition({ 0,0 }), CellPosition({ 0,1 })), CellPosition({ 1,1 }), false);
		ASSERT_FALSE(result);
	}

	TEST_F(GameSolverTests, isWallOnLine_checkHorizontalLine1)
	{
		using GameDeskObjects::Object_BetweenCells;
		using GameDeskObjects::CellPosition;
		const bool result = isWallOnLine(Object_BetweenCells(CellPosition({ 0,0 }), CellPosition({ 1,0 })), CellPosition({ 0, 1 }), true);
		ASSERT_FALSE(result);
	}
}