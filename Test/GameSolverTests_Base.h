#pragma once

#include <gtest/gtest.h>
#include "GameSolver.h"

namespace test
{
	/** @brief ������� ����� ��� ������� ������ GameSolver'� */
	class GameSolverTests_Base : public ::testing::Test
	{
	protected:
		/** @brief ������� ����� 2�2 �� �������� ����� �������� */
		static GameDesk createFakeDeskWithWalls()
		{
			auto desk = createFakeDeskWithoutWalls();
			using GameDeskObjects::Object_BetweenCells;
			desk.setWalls({
				Object_BetweenCells({ 0, 0 },{ 0, 1 }),
				Object_BetweenCells({ 1, 0 },{ 1, 1 }),
				Object_BetweenCells({ 0, 0 },{ 1, 0 }),
				Object_BetweenCells({ 0, 1 },{ 1, 1 }),
			});
			return desk;
		}

		/** @brief ������� ����� 2�2 ��� ������ */
		static GameDesk createFakeDeskWithoutWalls()
		{
			GameDesk desk;
			desk.setSizeOfDeskSquare(2);
			return desk;
		}

		/** @brief ������� GameSolver � ��������� ������ */
		static GameSolver createSolverWithDesk(const GameDesk& desk)
		{
			return GameSolver(desk);
		}
	};

	using GameDeskObjects::Move;
} // namespace test