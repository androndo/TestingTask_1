#include <gtest/gtest.h>
#include "GameDesk.h"

namespace test
{
	/** @brief GameDeskTests ����� ������ ��� ������� ����� */
	class GameDeskTests : public ::testing::Test
	{
	};

	using namespace GameDeskObjects;

	TEST_F(GameDeskTests, Initialize)
	{
		GameDesk desk;
		ASSERT_EQ(desk.getWalls().size(), 0);
		ASSERT_EQ(desk.getBalls().size(), 0);
		ASSERT_EQ(desk.getHoles().size(), 0);
		ASSERT_EQ(desk.getSizeOfDeskSquare(), 0);
	}

} // namespace test