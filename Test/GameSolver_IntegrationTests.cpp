#include "GameSolverTests_Base.h"
#include "GameDeskFactory.h"
#include "Common.h"

namespace test
{
	/** @brief GameSolverIntegrationTests  �������������� ����� � ��� ������, ��� ��������� ����� ����� �������� �������. */
	class GameSolverIntegrationTests : public GameSolverTests_Base
	{
	};

	TEST_F(GameSolverIntegrationTests, SampleGame_getAllPathsToWin)
	{
		// Arrange
		std::istringstream sample(getSampleConfig());
		auto desk = GameDeskFactory::createGameDesk(sample);
		GameSolver solver(desk);

		// Act
		auto paths = solver.getAllPathsToWin(desk);

		// Assert
		ASSERT_EQ(paths.size(), 5);
	}

	TEST_F(GameSolverIntegrationTests, SampleGame_getMinimalPathsToWin)
	{
		// Arrange
		std::istringstream sample(getSampleConfig());
		auto desk = GameDeskFactory::createGameDesk(sample);
		GameSolver solver(desk);

		// Act
		auto paths = solver.getMinimalPathsToWin();

		// Assert
		/** @note � ������� ���� �������� � ������� 3 �������,
		� ��� ��������� �� ��� �� ����� ���� 4 */
		ASSERT_EQ(paths.size(), 4);
	}

} // namespace test